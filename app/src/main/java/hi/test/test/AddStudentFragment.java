package hi.test.test;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hi.test.test.database.AppDatabase;
import hi.test.test.database.Students;


public class AddStudentFragment extends Fragment implements View.OnClickListener {
    FragmentActivity listener;
    EditText firstname;
    EditText lastname;
    EditText score;
    AppDatabase appDatabase;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appDatabase = Room.databaseBuilder(getActivity(), AppDatabase.class, "Students_data").allowMainThreadQueries().build();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_student, parent, false);
        firstname = (EditText)view.findViewById(R.id.firstname);
        lastname = (EditText)view.findViewById(R.id.lastname);
        score = (EditText)view.findViewById(R.id.score);
        Button button2 =(Button)view.findViewById(R.id.add_button);
        button2.setOnClickListener((View.OnClickListener) this);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.add_button:
                Toast.makeText(listener, "ok", Toast.LENGTH_SHORT).show();
                List<Students> userList = new ArrayList<>();
                userList.add(new Students(firstname.getText().toString(),lastname.getText().toString(),score.getText().toString()));
                appDatabase.studentDao().insert(userList);
                break;
        }
    }
    public String getName(){
        String name1 = firstname.getText().toString();
        return name1;
    }
    public String getlastname(){
        String age1 = lastname.getText().toString();
        return age1;
    }
    public String getscore(){
        String gender1 = score.getText().toString();
        return gender1;
    }
}