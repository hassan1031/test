package hi.test.test;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RemoteViews;

import java.util.ArrayList;
import java.util.List;

import hi.test.test.ListView.CustomAdapter;
import hi.test.test.ListView.Item;
import hi.test.test.database.AppDatabase;
import hi.test.test.database.Students;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawer;
    AppDatabase appDatabase;
    ListView mList;
    ArrayList<Item> arrayItem;
    AddStudentFragment addStudentFragment;
    EditText firstname;
    EditText lastname;
    EditText score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mList = (ListView) findViewById(R.id.list_view);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        appDatabase = Room.databaseBuilder(this , AppDatabase.class, "Students_data").allowMainThreadQueries().build();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_Sort) {
            sortlist();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            toolbar.setTitle("صفحه اصلی");
            mList.setVisibility(View.GONE);
        } else if (id == R.id.nav_students) {
            toolbar.setTitle("لیست دانشجویان");
            mList.setVisibility(View.VISIBLE);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frameLayout, new StudentsFragment());
            ft.commit();
            listView();
        } else if (id == R.id.nav_add_students) {
            toolbar.setTitle("افزودن دانشجو");
            mList.setVisibility(View.GONE);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frameLayout, new AddStudentFragment());
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void sortlist(){
        arrayItem = new ArrayList<>();
        arrayItem.clear();
        List<Students> studentsList = new ArrayList<>();
        studentsList = appDatabase.studentDao().getAllOrdered();
        for(int i = 0; i < studentsList.size(); i++){
            arrayItem.add(new Item("index", studentsList.get(i).getFirstname(), studentsList.get(i).getLastname(), studentsList.get(i).getScore()));
        }
        CustomAdapter mAdapter = new CustomAdapter(this, arrayItem);
        mList.setAdapter(mAdapter);
    }

    public void listView(){
        arrayItem = new ArrayList<>();
        List<Students> studentsList = new ArrayList<>();
        studentsList = appDatabase.studentDao().getAll();
        for(int i = 0; i < studentsList.size(); i++){
            arrayItem.add(new Item("index", studentsList.get(i).getFirstname(), studentsList.get(i).getLastname(), studentsList.get(i).getScore()));
        }
        CustomAdapter mAdapter = new CustomAdapter(this, arrayItem);
        mList.setAdapter(mAdapter);
    }

}
