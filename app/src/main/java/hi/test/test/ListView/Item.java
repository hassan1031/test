package hi.test.test.ListView;

public class Item {
    private String imgAvatar;
    private String firstname;
    private String lastname;
    private String score;

    public Item(String imgAvatar, String firstname, String lastname, String score) {
        this.imgAvatar = imgAvatar;
        this.firstname = firstname;
        this.lastname = lastname;
        this.score = score;
    }

    public String getImgAvatar() {
        return imgAvatar;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getScore() {
        return score;
    }

    public void setImgAvatar(String imgAvatar) {
        this.imgAvatar = imgAvatar;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
