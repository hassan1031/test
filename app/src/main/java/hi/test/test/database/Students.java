package hi.test.test.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Students {

    @PrimaryKey(autoGenerate = true)
    private int ID;
    @ColumnInfo(name = "fistname")
    private String firstname;
    @ColumnInfo(name = "lastname")
    private String lastname;
    @ColumnInfo(name = "score")
    private String score;

    public Students(String firstname, String lastname, String score) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.score = score;
    }

    public int getID() {
        return ID;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getScore() {
        return score;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
