package hi.test.test.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface StudentDao {

    @Query("SELECT * FROM Students ORDER BY score DESC")
    List<Students> getAllOrdered();

    @Query("SELECT * FROM Students")
    List<Students> getAll();

    @Query("SELECT * FROM Students where fistname LIKE  :firstName AND lastname LIKE :lastName")
    List<Students> findByName(String firstName, String lastName);

    @Query("SELECT COUNT(*) from Students")
    int countStudents();

    @Insert
    void insert(List<Students> mylist);

    @Delete
    void delete(List<Students> mylist);
}
